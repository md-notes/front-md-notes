module.exports = {
    outputDir: 'dist',

    publicPath: '/',

    transpileDependencies: [
      'vuetify'
    ]
}
