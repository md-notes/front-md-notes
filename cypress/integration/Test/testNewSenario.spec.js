describe('New Scenario (user, leaf, note)', () => {

  const uuid = () => Cypress._.random(0, 1e6)
  const id = uuid()

  it('Register', () => {

    cy.visit('http://localhost:8080/#/')

    cy.contains('Register').click()

    cy.url().should('include', '/register')

    cy.get('.v-input').first()
      .type(id)
      .should('have.value', '')

    cy.get('.v-input').last()
      .type('test')
      .should('have.value', '')

    cy.get('button').contains('Register').click()

  })

  it('Login', () => {
    cy.contains('Login').click()

    cy.url().should('include', '/login')

    cy.get('.v-input').first()
      .type(id)
      .should('have.value', '')

    cy.get('.v-input').last()
      .type('test')
      .should('have.value', '')

    cy.get('button').contains('Login').click()

    cy.url().should('equal', 'http://localhost:8080/#/')
  })

  it('New leaf', () => {
    cy.get('#open-button').click()

    cy.get('#leaf-button').click()

    cy.get('#dialog-label').type('My first Leaf')

    cy.get('#confirm').click()

  })

  it('New note', () => {

    cy.get('.node').click()

    cy.get('#open-button').click()

    cy.get('#markdown-button').click()

    cy.get('#dialog-label').type('My first Markdown')

    cy.get('#confirm').click()
  })

})
