FROM node:lts-alpine as build-stage
WORKDIR /app
ARG ENVIRONMENT
RUN echo $ENVIRONMENT
COPY package*.json ./
RUN npm install
COPY . .
RUN npx vue-cli-service build --mode $ENVIRONMENT

FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
