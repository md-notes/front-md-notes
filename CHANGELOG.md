# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.1](https://gitlab.com/md-notes/front-md-notes/compare/v0.1.0...v0.1.1) (2022-01-06)


### Bug Fixes

* add environment ([dee80bf](https://gitlab.com/md-notes/front-md-notes/commit/dee80bf104efd1007f947605c72196ad2fec393f))
* environments ([2fbb447](https://gitlab.com/md-notes/front-md-notes/commit/2fbb4476ffcbca40be2b68611021231585ed2243))
* fin test login register ([06c4c97](https://gitlab.com/md-notes/front-md-notes/commit/06c4c9747ebe5078b5de6569fa194ee6f1a14b13))
* testConnect ([d0532df](https://gitlab.com/md-notes/front-md-notes/commit/d0532df96df4bae4a586dbbb43ff0754444e41d2))
* **test:** some adjustments ([3deb3cc](https://gitlab.com/md-notes/front-md-notes/commit/3deb3cc779e4cc1889570af3a6007bbc69a2240c))

## 0.1.0 (2022-01-06)


### Bug Fixes

* add commitlint ([b17399a](https://gitlab.com/md-notes/front-md-notes/commit/b17399ac62fce68769b26cda57bce47c37bb80e2))
